/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package park;

import java.util.List;
import park.SelectedAttraction;

/**
 *
 * @author Shafa
 */
public class Registration_2 implements Registration{
    private int registrationId;
    private String visitorName;
    
    
    public Registration_2(int registrationId, String visitorName){
        this.registrationId = registrationId;
        this.visitorName = visitorName;
    }
    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return
     */
    public int getRegistrationId() {
        return registrationId;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return
     */
    
    public String getVisitorName() {
        return visitorName;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name  name of visitor
     * @return
     */
    @Override
    public String setVisitorName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return
     */
    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.        
        //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected  the attraction
     * @return {@code true} if the attraction is successfully added into the
     *     list, {@code false} otherwise
     */
    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
    
}
