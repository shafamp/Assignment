/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pet;

import Animal.Animals;
import java.util.ArrayList;
import java.util.Random;


/**
 *
 * @author Shafa
 */
public class Cats extends Animals {
    
    public Cats(String name, int bodyLength){
        
        super(name, bodyLength);
        
    }
    
    public ArrayList <Cats> getCats(){
        
        return getCats();
    }
    
    public char type(){

            if (getBodyLength()<45){
               return 'A';
            }else if(getBodyLength()<60){
                return 'B';
            }else
                return 'C';
    }

    
    public void furBrushed(){
        
        System.out.println("Nyaaan...");
    }
    
    public void cuddled(){
        
        Random rdm = new Random();
        String[] voiceList = {"Miaaaw..", "Purrr..", "Mwaw!","Mraaawr!"};
        int index = rdm.nextInt(4);
        System.out.println(voiceList[index]);
    }
    
}