/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package park;

import animal.Animal;
import java.util.List;

/**
 *
 * @author Shafa
 */
public interface SelectedAttraction {

    /**
     * Returns the name of attraction.
     *
     * @return
     */
    String getName();

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return
     */
    String getType();

    /**
     * Returns all performers of this attraction.
     *
     * @return
     */
    List<Animal> getPerformers();

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     *     performers, {@code false} otherwise
     */
    boolean addPerformer(Animal performer);
}

