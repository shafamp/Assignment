/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package a1station;

/**
 *
 * @author Emerio
 */
public class TrainCar {
    
    private WildCat cat;
    private TrainCar next;
    
    public TrainCar (WildCat cat){
        
      this.cat = cat;
    }
    
   public TrainCar (WildCat cat , TrainCar next){
       
      this.cat = cat;
      this.next = next;
    }
   
   public TrainCar (){
       
    }
   
   public double computeTotalWeight(){
       
       double totalWeight = 0;
       if(this.next != null){
            totalWeight = this.cat.getWeight()+this.next.computeTotalWeight();
       }
       else{
           totalWeight = this.cat.getWeight();
       }
       return totalWeight;
   }
   
   public double computeTotalMassIndex(){
       
       double totalMassIndex = 0;
       
       if(this.next != null){
            totalMassIndex = this.cat.computeMassIndex()+this.next.computeTotalMassIndex();
        }
       else{
           totalMassIndex = this.cat.computeMassIndex();
       }
       return totalMassIndex;
   }
   
   public void printCar(){
       
       System.out.println("The train departs to Javari Park");
       
       double berat = this.computeTotalMassIndex()/this.NumberTrain();
       //System.out.println("[LOCO]<--"+this.CatNameReverse());
       System.out.println("[LOCO]<--"+this.CatName());
       System.out.println("Average mass index of all cats :" + String.format("%.2f",berat));
       System.out.println("In average, the cats in the train are"+" *"+category(berat)+"*");
       
   }
   
  public static String category(double average){
      
    if(average < 18.5){
       
        return "underweight";
    }
    else if(average <= 25){
       
       return "normal";
    }
    else if (average >= 25){
       
       return "overweight";
    }
    else{
       
        return "obses";
    }
  }   
   
   public String CatName(){ 
       
       String allCatName = "";
       if(this.next == null){
           allCatName = "("+this.cat.getName()+")";
       }
       else{
           allCatName = "("+this.cat.getName()+")"+"--"+this.next.CatName();
       }
       return allCatName;
   }
   
   public String CatNameReverse(){
       
       String allCatName = "";
       if(this.next == null){
           allCatName = "("+this.cat.getName()+")";
       }
       else{
           allCatName = this.next.CatName()+"--"+"("+this.cat.getName()+")";
       }
       return allCatName;
   }
   
      public int NumberTrain(){
          
       int totalTrain = 0;
       if(this.next != null){
           
            totalTrain = 1+this.next.NumberTrain();
       }
       else{
           totalTrain = 1;
       }
       return totalTrain;
   }

}
