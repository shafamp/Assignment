/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Math.pow;

/**
 *
 * @author Emerio
 */
public class WildCat {
    
    private String name;
    private double weight;
    private double length;
    
    
    public WildCat(String name, double weight, double length){
        
        this.name = name;
        this.weight = weight;
        this.length = length;
    }
    
    
   public double computeMassIndex(){
       
        double BMI = 0;
        double panjang = this.length/100;
        BMI = (this.weight / (pow(panjang,2)));
        return BMI;
    }

    public String getName() {
        
        return name;
    }

    public double getWeight() {
        
        return weight;
    }

    public double getLength() {
        
        return length;
    }

    public void setName(String name) {
        
        this.name = name;
    }

    public void setWeight(double weight) {
        
        this.weight = weight;
    }

    public void setLength(double length) {
        
        this.length = length;
    }
}
