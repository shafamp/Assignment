/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reader;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;



public class CsvReader {

    public static final String COMMA = ",";
    
    private final Path file;
    protected final List<String> lines;
    
    private final String FILE_ATTRACTION = "animals_attractions.csv";
    private final String FILE_CATEGORIES = "animals_categories.csv";
    private final String FILE_RECORDS = "animals_records.csv";
    
    private String[] sections = {"Explore the Mammals", "World of Aves", "Reptillian Kingdom"};
    private String[] attractions = {"Circles of Fires", "Dancing Animals", "Counting Masters", "Passionate Coders"};
    private String[] categories = {"Explore the Mammals", "World of Aves", "Reptillian Kingdom"};
    private String[] records = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"};

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    public CsvReader(Path file) throws IOException {
        this.file = file;
        this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    }

    /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public long countValidRecords(){
        return getSection().size();
    }
         
    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidRecords(){
        
        List<String> falseSection = new ArrayList<String>();
        
        for(String line : lines) {
            boolean sectionCondition = false;
            String[] line_split = line.split(COMMA);  
            
            if(file.getFileName().toString().equals(FILE_CATEGORIES)){
                for(String section : sections){
                    if(line_split[2].equals(section)) {
                    sectionCondition = true;
                    }
                }                  
                if(sectionCondition == false) {
                    if(!falseSection.contains(line_split[2])){
                        falseSection.add(line_split[2]);
                    }
                }
            }
            
            if(file.getFileName().toString().equals(FILE_ATTRACTION)){
                for(String attraction : attractions){                    
                    if(line_split[1].equals(attraction)) {
                    sectionCondition = true;
                    }
                }                
                if(sectionCondition == false) {
                    if(!falseSection.contains(line_split[1])){
                        falseSection.add(line_split[1]);
                    } System.out.println(falseSection.size());
                }
            }            
            
            if(file.getFileName().toString().equals(FILE_CATEGORIES)){
                for(String categorie : categories){
                    if(line_split[2].equals(categorie)) {
                    sectionCondition = true;
                    }
                }                  
                if(sectionCondition == false) {
                    if(!falseSection.contains(line_split[2])){
                        falseSection.add(line_split[2]);
                    }
                }
            }
            
            if(file.getFileName().toString().equals(FILE_RECORDS)){
                    if(line_split[0].contains(line_split[0])){
                    sectionCondition = true;
                    }
                                 
                if(sectionCondition == false) {
                    if(!falseSection.contains(line_split[0])){
                        falseSection.add(line_split[0]);
                    }
                }
            }
        }
            return falseSection.size();
    }

    
    public List<String> getSection() {
        
        List<String> trueSection = new ArrayList<String>();
        
        for(String line : lines) {
            
            String[] line_split = line.split(COMMA);  
            
            if(file.getFileName().toString().equals(FILE_CATEGORIES)){
                for(String section : sections){
                    if(line_split[2].equals(section)) {
                        if(!trueSection.contains(line_split[2])){
                        trueSection.add(line_split[2]);
                    }
                }
            }
        }
            
            if(file.getFileName().toString().equals(FILE_ATTRACTION)){
                for(String attraction : attractions){                    
                    if(line_split[1].equals(attraction)) {
                    if(!trueSection.contains(line_split[1])){
                        trueSection.add(line_split[1]);
                    } 
                }
            }    
            }
            
            if(file.getFileName().toString().equals(FILE_CATEGORIES)){
                for(String categorie : categories){
                    if(line_split[1].equals(categorie)) {
                    if(!trueSection.contains(line_split[1])){
                        trueSection.add(line_split[1]);
                    }
                }
            }
            }
            
            if(file.getFileName().toString().equals(FILE_RECORDS)){
                    if(line_split[0].contains(line_split[0])){
                    if(!trueSection.contains(line_split[0])){
                        trueSection.add(line_split[0]);
                    }
                }
            }
        }
            return trueSection;
        }
    }

