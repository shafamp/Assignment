/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animal;

/**
 *
 * @author Emerio
 */
    
    public class Animals {
    
    private String name;
    private int bodyLength;
    
    public Animals(String name, int bodyLength){
        
        this.name = name;
        this.bodyLength = bodyLength;
        
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBodyLength() {
        return bodyLength;
    }

    public void setLength(int bodyLength) {
        this.bodyLength = bodyLength;
    }
    
    }
