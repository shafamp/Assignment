/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pet;

import Animal.Animals;

/**
 *
 * @author Shafa
 */
public class Parrots extends Animals {
    
    public Parrots(String name, int bodyLength){
        
        super(name, bodyLength);
    }
    
    public char type(){

            if (getBodyLength()<45){
                
               return 'A';
            }else if(getBodyLength()<60){
                
                return 'B';
            }else
                return 'C';
    }
    
    public void fly(){
        
        System.out.println("TERBAAANGGGGG....");
    }
    
    
    public void mumble(String word){
        
        System.out.println(word.toUpperCase());
    }

}

