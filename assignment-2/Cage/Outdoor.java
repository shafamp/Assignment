/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cage;

import Wild.Eagle;
import Wild.Lion;
import java.util.ArrayList;

/**
 *
 * @author Emerio
 */
public class Outdoor {
    
     public static String type;
    // public String location="indoor";
     
    public static void arrangeCage(ArrayList<Eagle>eagles, String location) {
        
        if (eagles.size() != 0) {
            
            System.out.println("Location: " + location);

            int banyak = eagles.size() / 3;
            int Level = 3;
            int posisi = eagles.size() - 1;
            if (eagles.size() == 2) {
                posisi = 1;
            } else if (eagles.size() == 1) {
                posisi = 0;
            }
            
            Eagle[][] array = new Eagle[3][];
            
            for (int x = 0; x < 3; x++) {
                if (eagles.size() >= 3) {
                    if (eagles.size() % 3 == 2 && (x == 0 || x == 1)) {
                        array[x] = new Eagle[banyak + 1];
                    } else if (eagles.size() % 3 == 1 && x == 0) {
                        array[x] = new Eagle[banyak + 1];
                    } else {
                        array[x] = new Eagle[banyak];
                    }
                } else if (eagles.size() < 3) {
                    array[x] = new Eagle[1];
                }

                for (int b = 0; b < array[x].length; b++) {
                    if ((eagles.size() == 2 && x == 0) || eagles.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    array[x][b] = eagles.get(posisi);
                    posisi--;
                }
                
                int y = array[x].length - 1;
                System.out.print("Level " + Level + ": ");
                for (int ii = 0; ii < array[x].length; ii++) {
                    if ((eagles.size() == 2 && x == 0) || eagles.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                    y--;
                }
                Level--;
                System.out.println();
            }
            
            System.out.println();
            System.out.println("After rearrangement...");
            Level = 3;
            for (int x = 1; x <= array.length; x++) {
                System.out.print("Level " + Level + " : ");
                if (x == 3) {
                    if (eagles.size() < 3) {
                        break;
                    }
                    for (int y = 0; y < array[0].length; y++) {
                        System.out.printf(array[0][y].getName() + " " + "(%d - %c), ", array[0][y].getBodyLength(), array[0][y].type());
                    }
                    break;
                }
                for (int y = 0; y < array[x].length; y++) {
                    if (eagles.size() == 1 && x == 1) {
                        System.out.printf(array[2][y].getName() + " " + "(%d - %c), ", array[2][y].getBodyLength(), array[2][y].type());
                        break;
                    } else if (eagles.size() == 1 && x == 2 || x == 3) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                }
                Level--;
                System.out.println();

            }
        }
    }

    public static void arrangeCage2(ArrayList<Lion>lions, String location) {
        
        if (lions.size() != 0) {
            System.out.println("Location: " + location);
            
            int banyak = lions.size() / 3;
            int Level = 3;
            int posisi = lions.size() - 1;
            if (lions.size() == 2) {
                posisi = 1;
            } else if (lions.size() == 1) {
                posisi = 0;
            }
            Lion[][] array = new Lion[3][];
            for (int x = 0; x < 3; x++) {
                if (lions.size() >= 3) {
                    if (lions.size() % 3 == 2 && (x == 0 || x == 1)) {
                        array[x] = new Lion[banyak + 1];
                    } else if (lions.size() % 3 == 1 && x == 0) {
                        array[x] = new Lion[banyak + 1];
                    } else {
                        array[x] = new Lion[banyak];
                    }
                } else if (lions.size() < 3) {
                    array[x] = new Lion[1];
                }

                for (int b = 0; b < array[x].length; b++) {
                    if ((lions.size() == 2 && x == 0) || lions.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    array[x][b] = lions.get(posisi);
                    posisi--;
                }
                int y = array[x].length - 1;
                System.out.print("Level " + Level + ": ");
                for (int ii = 0; ii < array[x].length; ii++) {
                    if ((lions.size() == 2 && x == 0) || lions.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                    y--;
                }
                Level--;
                System.out.println();
            }
            System.out.println();
            System.out.println("After rearrangement...");
            Level = 3;
            for (int x = 1; x <= array.length; x++) {
                System.out.print("Level " + Level + " : ");
                if (x == 3) {
                    if (lions.size() < 3) {
                        break;
                    }
                    for (int y = 0; y < array[0].length; y++) {
                        System.out.printf(array[0][y].getName() + " " + "(%d - %c), ", array[0][y].getBodyLength(), array[0][y].type());
                    }
                    break;
                }
                for (int y = 0; y < array[x].length; y++) {
                    if (lions.size() == 1 && x == 1) {
                        System.out.printf(array[2][y].getName() + " " + "(%d - %c), ", array[2][y].getBodyLength(), array[2][y].type());
                        break;
                    } else if (lions.size() == 1 && x == 2 || x == 3) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                }
                Level--;
                System.out.println();

            }
        }
    }
    
}