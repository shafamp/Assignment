/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author Emerio
 */
public class Mammals extends Animal{ 
    
    private String specialCondition;
    private static final String PREGNANT = "pregnant";
    
    
    public Mammals (Integer id, String type, String name, Gender gender, double length, double weight, Condition condition, String specialCondition){
        super(id, type, name, gender, length, weight, condition);
        this.specialCondition = specialCondition;
    }

    protected boolean specificCondition() {
        if(this.specialCondition.equals(PREGNANT)){
            return false;
        } else {
            return true;
        }
    }  
    
}
