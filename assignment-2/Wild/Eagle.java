/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Wild;

import Animal.Animals;

/**
 *
 * @author Shafa
 */
public class Eagle extends Animals{
    
    public Eagle(String name, int bodyLength){
    
        super(name, bodyLength);
    }
    
    public char type(){

            if (getBodyLength()<45){
                
               return 'A';
            }else if(getBodyLength()<60){
                
                return 'B';
            }else
                return 'C';
    }
 
    public void eagleVoice(){
        
        System.out.println("Kwaaakkk");
        System.out.println("You hurt!");
    }

}

