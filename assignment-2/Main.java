/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Cage.Indoor;
import Cage.Outdoor;
import Pet.Cats;
import Pet.Hamsters;
import Pet.Parrots;
import Wild.Eagle;
import Wild.Lion;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList <Cats> cats= new ArrayList<Cats>();
        ArrayList <Eagle> eagles= new ArrayList<Eagle>();
        ArrayList <Hamsters> hamsters = new ArrayList<Hamsters>();
        ArrayList <Parrots> parrots = new ArrayList<Parrots>();
        ArrayList <Lion> lions = new ArrayList<Lion>();



        System.out.println("Welcome to Javari Park");
        System.out.println("input the number of animals");



            System.out.print("Cat: ");
            Scanner input = new Scanner(System.in);
            int jlhKucing = Integer.parseInt(input.next());

            if (jlhKucing != 0) {
                System.out.println("Provide the information of cat(s): ");
                String masukan = input.next();

                String[] pisah = masukan.split(",");
                for (int i=0; i<pisah.length;i++){
                    String [] a = pisah[i].split("\\|");
                    String nama = a[0];
                    int panjang = Integer.parseInt(a[1]) ;
                    Cats kucing = new Cats(nama, panjang);
                    cats.add(kucing);
                    //System.out.println();
                }

            }

            System.out.print("Lion: ");
            int jlhSinga = Integer.parseInt(input.next());
             if (jlhSinga != 0) {
            System.out.println("Provide the information of lion(s): ");
            String masukan = input.next();

            String[] pisah = masukan.split(",");
            for (int i=0; i<pisah.length;i++){
                String [] a = pisah[i].split("\\|");
                String nama = a[0];
                int panjang = Integer.parseInt(a[1]) ;
                Lion singa = new Lion(nama, panjang);
                lions.add(singa);

            }

        }

        System.out.print("Eagle: ");
        int jlhEagle = Integer.parseInt(input.next());
        if (jlhEagle != 0) {
            System.out.println("Provide the information of eagle(s): ");
            String masukan = input.next();

            String[] pisah = masukan.split(",");
            for (int i=0; i<pisah.length;i++){
                String [] a = pisah[i].split("\\|");
                String nama = a[0];
                int panjang = Integer.parseInt(a[1]) ;
                Eagle elang = new Eagle(nama, panjang);
                eagles.add(elang);

            }
        }

            System.out.print("Parrot: ");
            int jlhParrot = Integer.parseInt(input.next());
        if (jlhParrot != 0) {
            System.out.println("Provide the information of parrot(s): ");
            String masukan = input.next();

            String[] pisah = masukan.split(",");
            for (int i=0; i<pisah.length;i++){
                String [] a = pisah[i].split("\\|");
                String nama = a[0];
                int panjang = Integer.parseInt(a[1]) ;
                Parrots beo = new Parrots(nama, panjang);
                parrots.add(beo);

            }

        }
            System.out.print("Hamster: ");
            int jlhHamster = Integer.parseInt(input.next());
        if (jlhHamster != 0) {
            System.out.println("Provide the information of hamster(s): ");
            String masukan = input.next();

            String[] pisah = masukan.split(",");
            for (int i=0; i<pisah.length;i++){
                String [] a = pisah[i].split("\\|");
                String nama = a[0];
                int panjang = Integer.parseInt(a[1]) ;
                Hamsters marmut = new Hamsters(nama, panjang);
                hamsters.add(marmut);

            }

        }

        System.out.println("Animals has been successfully recorded");
        System.out.println("=================================================================");


        System.out.println("Cage arrangement:");
        System.out.println();
        Indoor.arrangeCage(cats,"indoor");
        System.out.println();
        Outdoor.arrangeCage(eagles, "outdoor");
        System.out.println();
        Indoor.arrangeCage2(parrots,"indoor");
        System.out.println();
        
        Outdoor.arrangeCage2(lions,"outdoor");
        System.out.println();
        Indoor.arrangeCage3(hamsters,"indoor");
        System.out.println();

        System.out.println("=================================================================");
        while(true){
          
            System.out.println("Which animal you want visit? ");
            System.out.println("1: Cat 2 : Eagle  3 : Hamster 4: Parrot 5 : Lion 99:exit");

            int pilihan = Integer.parseInt(input.next());
            if (pilihan == 1) {
                System.out.print("Mention the name of cat you want to visit: ");
                String nama = input.next();
                boolean isKetemu=false;
                for (Cats cats1 : cats){
                    if (cats1.getName().equals(nama)) {
                        System.out.println("You are visiting " + nama + " (cat) now, what would you like to do? ");
                        System.out.println("1: Brush the fur 2:Cuddle");
                        int a = Integer.parseInt(input.next());
                        if (a == 1) {
                            System.out.println("Time to clean "+nama+" 's fur");
                            System.out.print(nama + " makes a voice: ");
                            cats1.furBrushed();

                        } else if (a == 2) {
                            System.out.print(nama + " makes a voice: ");
                            cats1.cuddled();
                        } else {
                            System.out.println("You do nothing!");
                        }
                        System.out.println("Back to the office");
                        isKetemu = true;
                        break;

                    }
                }
                if(!isKetemu){
                    System.out.println("There is no cat with that name! Back to the office");

                }
            }


            if (pilihan == 2) {
                System.out.print("Mention the name of eagle you want to visit: ");
                String nama = input.next();
                boolean isKetemu=false;
                for (Eagle eagles1 : eagles) {
                    if (eagles1.getName().equals(nama)) {
                        System.out.println("You are visiting " +nama+" (eagle) now, what would you like to do? ");
                        System.out.println("1: Order to fly");
                        int a = Integer.parseInt(input.next());
                        if (a == 1) {
                            System.out.println("Order to fly");
                            System.out.print(nama + " makes a voice: ");
                            eagles1.eagleVoice();
                        } else {
                            System.out.println("You do nothing!");
                        }
                        System.out.println("Back to the office");
                        isKetemu = true;
                        break;

                    }
                    if (!isKetemu){
                        System.out.println("There is no eagle with that name! Back to the office");
                    }
                }
            }


            if (pilihan == 3) {
                System.out.print("Mention the name of hamster you want to visit: ");
                String nama = input.next();
                boolean isKetemu = false;
                for (Hamsters hamster1 : hamsters)
                    if (hamster1.getName().equals(nama)) {
                        System.out.println("You are visiting " +nama+" (hamster) now, what would you like to do? ");
                        System.out.println("1: See it gnawing 2:Order to run in the hamster wheel");
                        int a = Integer.parseInt(input.next());
                        if (a == 1) {

                            System.out.print(nama + " makes a voice: ");
                            hamster1.gnaw();
                        } else if (a == 2) {
                            System.out.print(nama + " makes a voice: ");
                            hamster1.runOnWheel();
                        } else {
                            System.out.println("You do nothing!");
                        }
                        System.out.println("Back to the office");
                        isKetemu = true;
                        break;

                    }
                if (!isKetemu) {
                    System.out.println("There is no hamster with that name! Back to the office");
                }
            }



            if (pilihan == 4) {
                System.out.print("Mention the name of parrot you want to visit: ");
                String nama = input.next();
                boolean isKetemu = false;
                for (Parrots parrot1 : parrots)
                    if (parrot1.getName().equals(nama)) {
                        System.out.println("You are visiting "+nama+ " (parrot) now, what would you like to do? " + nama);
                        System.out.println("1: Order to fly 2:Do Conversation");
                        int a = Integer.parseInt(input.next());
                        if (a == 1) {
                            System.out.print("Parrot " + nama + "flies! ");
                            parrot1.fly();
                        } else if (a == 2) {
                            System.out.print("You say: ");
                            String kata = input.next();
                            System.out.print(nama + "makes a voice: ");
                            parrot1.mumble(kata);
                        } else {
                            System.out.println(nama + " says : HM?");
                        }
                        System.out.println("Back to the office");
                        isKetemu = true;
                        break;
                    }
            if (!isKetemu){
                System.out.println("There is no parrot with that name! Back to the office");
                }
            }


            if (pilihan == 5) {
                System.out.print("Mention the name of lion you want to visit: ");
                String nama = input.next();
                boolean isKetemu=false;
                for (Lion lion1 : lions)
                    if (lion1.getName().equals(nama)) {
                        System.out.println("You are visiting "+nama+" (lion) now, what would you like to do? " + nama);
                        System.out.println("1: See it hunting  2:Brush the mane 3: Disturb it");
                        int a = Integer.parseInt(input.next());
                        if (a == 1) {
                            System.out.println("Lion is hunting");
                            System.out.print(nama+" makes avoice");
                            lion1.hunt();
                        } else if (a==2){
                            System.out.print("Clean the lion's mane..");
                            System.out.print(nama + "makes a voice: ");
                            lion1.goodMood();
                        }else if (a==3){
                            System.out.print(nama + "makes a voice: ");
                            lion1.disturbed();
                        }else {
                            System.out.println("You do nothing!");
                        }
                        System.out.println("Back to the office");
                        isKetemu = true;
                        break;

                    }
                    if (!isKetemu){
                        System.out.println("There is no lion with that name! Back to the office");
                    }
            }

            if (pilihan==99){
                break;
            }  
        }
    }
}