/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package a1station;

import java.util.Scanner;

/**
 *
 * @author Emerio
 */
public class A1Station {
    
    private static final double THRESHOLD = 250; 
    
    public static void main(String[] args) {
        
        
       TrainCar trCar = new TrainCar();
       Scanner inputan = new Scanner(System.in);
       //System.out.println("Masukkan Jumlah Kucing :");
       
       Integer jumlahKucing = inputan.nextInt();
       inputan.nextLine();
       
       for(int i=0;i<jumlahKucing;i++){
           
           String input = inputan.next();
           String inputSplit [] = input.split(",");
           
           WildCat cat = new WildCat(inputSplit[0], Double.parseDouble(inputSplit[1]), Double.parseDouble(inputSplit[2])); 
           TrainCar car;
           
           if(i == 0){
               
               trCar = new TrainCar(cat);
           } 
           else {
               if (trCar.computeTotalWeight() + cat.getWeight()<= THRESHOLD){
                   trCar = new TrainCar(cat, trCar);
               } 
               else {     
                    //System.out.println(trCar.computeTotalMassIndex());
                   trCar.printCar();
                   trCar = new TrainCar(cat);
               }
           }
       }
       trCar.printCar(); 
       inputan.close();
    }
}
