/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cage;

import Pet.Cats;
import Pet.Hamsters;
import Pet.Parrots;
import java.util.ArrayList;

/**
 *
 * @author Emerio
 */
public class Indoor{
    
    
    public static String type;
   // public String location="indoor";
    public static void arrangeCage(ArrayList<Cats>cats, String location){
        
        if (cats.size()!= 0) {
            
            System.out.println("Location: "+location);
            int banyak = cats.size()/3;
            int Level = 3;
            int posisi = cats.size()-1;
            
            if (cats.size() == 2){
            posisi=1;
            }else if (cats.size() ==1){
                posisi=0;
            }
            
            Cats[][] array = new Cats[3][];
            
            for (int x=0;x<3;x++) {
                if (cats.size() >= 3) {
                    if (cats.size() % 3 == 2 && (x == 0 || x == 1)) {
                        array[x] = new Cats[banyak + 1];
                    } else if (cats.size() % 3 == 1 && x == 0) {
                        array[x] = new Cats[banyak + 1];
                    } else {
                        array[x] = new Cats[banyak];
                    }
                } else if (cats.size() < 3) {
                    array[x] = new Cats[1];
                }

            for (int b=0;b<array[x].length;b++){
                if ((cats.size()==2 && x==0) || cats.size()== 1 && (x==0 || x == 1)){
                    break;
                }
                array[x][b] = cats.get(posisi);
                posisi--;
            }
            
            int y =array[x].length-1;
            System.out.print("Level "+ Level +": ");
            
            for (int ii=0;ii<array[x].length;ii++){
                if ((cats.size()==2 && x==0) || cats.size()== 1 && (x==0 || x == 1)){
                    break;
                }
                System.out.printf(array[x][y].getName()+" "+"(%d - %c), ",array[x][y].getBodyLength(),array[x][y].type());
                y--;
            }
            Level--;
            System.out.println();
        }
        System.out.println();
        System.out.println("After rearrangement...");
        Level =3;
        for (int x=1;x<=array.length;x++){
            System.out.print("Level "+ Level+" : ");
            if (x==3){
                if(cats.size()<3){
                    break;
                }
                for (int y =0;y<array[0].length;y++){
                        System.out.printf(array[0][y].getName()+" "+"(%d - %c), ",array[0][y].getBodyLength(),array[0][y].type());
                    }break;
                }
                for (int y =0;y<array[x].length;y++){
                    if (cats.size()== 1 && x==1){
                        System.out.printf(array[2][y].getName()+" "+"(%d - %c), ",array[2][y].getBodyLength(),array[2][y].type());
                        break;
                    }else if(cats.size()==1 && x==2 || x== 3){
                        break;}
                    System.out.printf(array[x][y].getName()+" "+"(%d - %c), ",array[x][y].getBodyLength(),array[x][y].type());
                }
                Level--;
            System.out.println();

            }
        }
    }



    public static void arrangeCage2(ArrayList<Parrots>parrots, String location){
        
        if (parrots.size()!= 0) {
            System.out.println("Location: " + location);

            int banyak = parrots.size() / 3;
            int Level = 3;
            int posisi = parrots.size() - 1;
            if (parrots.size() == 2) {
                posisi = 1;
            } else if (parrots.size() == 1) {
                posisi = 0;
            }
            Parrots[][] array = new Parrots[3][];
            for (int x = 0; x < 3; x++) {
                if (parrots.size() >= 3) {
                    if (parrots.size() % 3 == 2 && (x == 0 || x == 1)) {
                        array[x] = new Parrots[banyak + 1];
                    } else if (parrots.size() % 3 == 1 && x == 0) {
                        array[x] = new Parrots[banyak + 1];
                    } else {
                        array[x] = new Parrots[banyak];
                    }
                } else if (parrots.size() < 3) {
                    array[x] = new Parrots[1];
                }

                for (int b = 0; b < array[x].length; b++) {
                    if ((parrots.size() == 2 && x == 0) || parrots.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    array[x][b] = parrots.get(posisi);
                    posisi--;
                }
                int y = array[x].length - 1;
                System.out.print("Level " + Level + ": ");
                for (int ii = 0; ii < array[x].length; ii++) {
                    if ((parrots.size() == 2 && x == 0) || parrots.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                    y--;
                }
                Level--;
                System.out.println();
            }
            System.out.println();
            System.out.println("After rearrangement...");
            Level = 3;
            for (int x = 1; x <= array.length; x++) {
                System.out.print("Level " + Level + " : ");
                if (x == 3) {
                    if (parrots.size() < 3) {
                        break;
                    }
                    for (int y = 0; y < array[0].length; y++) {
                        System.out.printf(array[0][y].getName() + " " + "(%d - %c), ", array[0][y].getBodyLength(), array[0][y].type());
                    }
                    break;
                }
                for (int y = 0; y < array[x].length; y++) {
                    if (parrots.size() == 1 && x == 1) {
                        System.out.printf(array[2][y].getName() + " " + "(%d - %c), ", array[2][y].getBodyLength(), array[2][y].type());
                        break;
                    } else if (parrots.size() == 1 && x == 2 || x == 3) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                }
                Level--;
                System.out.println();

            }
        }
    }

    public static void arrangeCage3(ArrayList<Hamsters>hamsters, String location) {
        
        if (hamsters.size() != 0) {
            System.out.println("Location: " + location);


            int banyak = hamsters.size() / 3;
            int Level = 3;
            int posisi = hamsters.size() - 1;
            if (hamsters.size() == 2) {
                posisi = 1;
            } else if (hamsters.size() == 1) {
                posisi = 0;
            }
            Hamsters[][] array = new Hamsters[3][];
            for (int x = 0; x < 3; x++) {
                if (hamsters.size() >= 3) {
                    if (hamsters.size() % 3 == 2 && (x == 0 || x == 1)) {
                        array[x] = new Hamsters[banyak + 1];
                    } else if (hamsters.size() % 3 == 1 && x == 0) {
                        array[x] = new Hamsters[banyak + 1];
                    } else {
                        array[x] = new Hamsters[banyak];
                    }
                } else if (hamsters.size() < 3) {
                    array[x] = new Hamsters[1];
                }

                for (int b = 0; b < array[x].length; b++) {
                    if ((hamsters.size() == 2 && x == 0) || hamsters.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    array[x][b] = hamsters.get(posisi);
                    posisi--;
                }
                int y = array[x].length - 1;
                System.out.print("Level " + Level + ": ");
                for (int ii = 0; ii < array[x].length; ii++) {
                    if ((hamsters.size() == 2 && x == 0) || hamsters.size() == 1 && (x == 0 || x == 1)) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                    y--;
                }
                Level--;
                System.out.println();
            }
            System.out.println();
            System.out.println("After rearrangement...");
            Level = 3;
            for (int x = 1; x <= array.length; x++) {
                System.out.print("Level " + Level + " : ");
                if (x == 3) {
                    if (hamsters.size() < 3) {
                        break;
                    }
                    for (int y = 0; y < array[0].length; y++) {
                        System.out.printf(array[0][y].getName() + " " + "(%d - %c), ", array[0][y].getBodyLength(), array[0][y].type());
                    }
                    break;
                }
                for (int y = 0; y < array[x].length; y++) {//fix
                    if (hamsters.size() == 1 && x == 1) {
                        System.out.printf(array[2][y].getName() + " " + "(%d - %c), ", array[2][y].getBodyLength(), array[2][y].type());
                        break;
                    } else if (hamsters.size() == 1 && x == 2 || x == 3) {
                        break;
                    }
                    System.out.printf(array[x][y].getName() + " " + "(%d - %c), ", array[x][y].getBodyLength(), array[x][y].type());
                }
                Level--;
                System.out.println();

            }
        }
    }

        
        
    
}
