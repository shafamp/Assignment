/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package animal;

/**
 *
 * @author Shafa
 */
public enum Condition {

    HEALTHY, SICK;
    private static final String HEALTHY_STR = "healthy";
    private static final String SICK_STR = "not healthy";

    /**
     * Returns the correct condition enum based on given string representation
     * of a condition.
     *
     * @param str   condition description
     * @return
     */
    public static Condition parseCondition(String str) {
        if (str.equalsIgnoreCase(HEALTHY_STR)) {
            return Condition.HEALTHY;
        } else if (str.equalsIgnoreCase(SICK_STR)) {
            return Condition.SICK;
        } else {
            throw new UnsupportedOperationException();
        }
    }
}


