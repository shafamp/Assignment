import animal.Animal;
import animal.Aves;
import animal.Condition;
import animal.Gender;
import animal.Mammals;
import animal.Reptiles;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import park.Registration;
import reader.CsvReader;
import static reader.CsvReader.COMMA;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author aisha
 */
public class Main {
    
    private static final String FILE_ATTRACTION = "animals_attractions.csv";
    private static final String FILE_CATEGORIES = "animals_categories.csv";
    private static final String FILE_RECORDS = "animals_records.csv";
    
    private static final String ExploreTheMammals = "Explore the Mammals";
    private static final String WorldOfAves = "World of Aves";
    private static final String ReptillianKingdom = "Reptillian Kingdom";
    
    public static List<Mammals> MammalList = new ArrayList<>();
    public static List<Aves> AvesList = new ArrayList<>();
    public static List<Reptiles> ReptileList = new ArrayList<>();
    
    public static List<String> ExploreTheMammalsAnimals = new ArrayList<>();
    public static List<String> WorldOfAvesAnimals = new ArrayList<>();
    public static List<String> ReptillianKingdomAnimals = new ArrayList<>();
    
    private static final String CirclesOfFiresAnim = "Circles of Fires";
    private static final String DancingAnimalsAnim = "Dancing Animals";
    private static final String CountingMastersAnim = "Counting Masters";
    private static final String PassionateCodersAnim = "Passionate Coders";
    
    public static List<String> CirclesOfFires = new ArrayList<>();
    public static List<String> DancingAnimals = new ArrayList<>();
    public static List<String> CountingMasters = new ArrayList<>();
    public static List<String> PassionateCoders = new ArrayList<>();
    
    
    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("... Opening default section database from data. ... File not found or incorrect file!\n");
        System.out.print("Please provide the source data path: ");
        
        Scanner scn = new Scanner(System.in);
        String masukan = scn.nextLine();
        
        Path path_attraction = FileSystems.getDefault().getPath(masukan, FILE_ATTRACTION);
        Path path_categories = FileSystems.getDefault().getPath(masukan, FILE_CATEGORIES);
        Path path_records = FileSystems.getDefault().getPath(masukan, FILE_RECORDS);
        
        CsvReader csvReader_attraction = new CsvReader(path_attraction);
        CsvReader csvReader_categories = new CsvReader(path_categories);
        CsvReader csvReader_records = new CsvReader(path_records);
        
        List<String> lines_attraction = csvReader_attraction.getLines();
        List<String> lines_categories = csvReader_categories.getLines();
        List<String> lines_records = csvReader_records.getLines();
        
       
        //choose animals
        for(String line_category : lines_categories){
            String[] line_split = line_category.split(COMMA);
            if(line_split[2].equals(ExploreTheMammals)){
                ExploreTheMammalsAnimals.add(line_split[0]);
            }
            if(line_split[2].equals(WorldOfAves)){
                WorldOfAvesAnimals.add(line_split[0]);
            }
            if(line_split[2].equals(ReptillianKingdom)){
                ReptillianKingdomAnimals.add(line_split[0]);
            }
        }
        
        //choose records
        for(String line_att : lines_attraction){
            String[] line_split = line_att.split(COMMA);
            if(line_split[1].equals(CirclesOfFiresAnim)){
                CirclesOfFires.add(line_split[0]);
            }
            if(line_split[1].equals(DancingAnimalsAnim)){
                DancingAnimals.add(line_split[0]);
            }
            if(line_split[1].equals(CountingMastersAnim)){
                CountingMasters.add(line_split[0]);
            }
             if(line_split[1].equals(PassionateCodersAnim)){
                PassionateCoders.add(line_split[0]);
            }
        }
        
        
       
        
//        for(String line_record : lines_records){
//            String[] line_split = line_record.split(COMMA);
//            int id = Integer.parseInt(line_split[0]);
//            String type = line_split[1];
//            String name = line_split[2];
//            Gender gender = Gender.parseGender(line_split[3]);
//            double length = Double.parseDouble(line_split[4]);
//            double weight = Double.parseDouble(line_split[5]);
//            String specialCondition = line_split[6];
//            Condition condition = Condition.parseCondition(line_split[7]);
//            if(ExploreTheMammalsAnimals.contains(type)){
//               MammalList.add(new Mammals(id, type, name, gender, length, weight, condition,specialCondition)); 
//            } 
//            
//            if(WorldOfAves.contains(type)){
//               AvesList.add(new Aves(id, type, name, gender, length, weight, condition,specialCondition)); 
//            } 
//            
//            if(ReptillianKingdomAnimals.contains(type)){
//               ReptileList.add(new Reptiles(id, type, name, gender, length, weight, condition,specialCondition)); 
//            } 
//            
//        }
//        
        
        
        
        
        System.out.println("\n... Loading... Success... System is populating data...");
        System.out.println("\nFound _" + csvReader_categories.countValidRecords() + "_ valid sections and _" + csvReader_categories.countInvalidRecords() + "_ invalid sections");
        System.out.println("Found _" + csvReader_attraction.countValidRecords() + "_ valid attractions and _" + csvReader_attraction.countInvalidRecords() + "_ invalid attractions");
        System.out.println("Found _" + csvReader_categories.countValidRecords() + "_ valid animal categories and _" + csvReader_categories.countInvalidRecords() + "_ invalid animal categories");
        System.out.println("Found _" + csvReader_records.countValidRecords() + "_ valid animal records and _" + csvReader_records.countInvalidRecords() + "_ invalid animal records");
        
        System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu\n");
        System.out.println("Javari Park has 3 sections:");
        
        //choose section
        List<String> section = csvReader_categories.getSection();
        for(int i = 0; i<section.size(); i++){
            System.out.println(i+1+". "+section.get(i));
        }
        
        System.out.print("Please choose your preferred section (type the number): "); 
        int input = scn.nextInt();
        List<String> selectedAnimal = new ArrayList<>();
        if (input == 1){
            System.out.println("\n--Explore the Mammals--");
            for(int i = 0; i<ExploreTheMammalsAnimals.size(); i++){                
                System.out.println(i+1+". "+ExploreTheMammalsAnimals.get(i));
                selectedAnimal = ExploreTheMammalsAnimals;
            }
        }else if (input == 2){
            System.out.println("\n--Explore the Aves--");
            for(int i = 0; i<WorldOfAvesAnimals.size(); i++){                
                System.out.println(i+1+". "+WorldOfAvesAnimals.get(i));
                selectedAnimal = WorldOfAvesAnimals;
            }
        }else if (input == 3){
            System.out.println("\n--Explore the Reptile-");
            for(int i = 0; i<ReptillianKingdomAnimals.size(); i++){                
                System.out.println(i+1+". "+ReptillianKingdomAnimals.get(i));
                selectedAnimal = ReptillianKingdomAnimals;
            }
        }else{
                System.out.println("Input tidak sesuai!");          
        }
        
        System.out.println("Please choose your preferred animals (type the number): ");
        int inputan = scn.nextInt();
        
        String selectAnimal = selectedAnimal.get(inputan-1);
        System.out.println(selectAnimal);
        
        List<String> selectedAttractions = new ArrayList<>();
        
        
        if(CirclesOfFires.contains(selectAnimal)){
            selectedAttractions.add("Circles Of Fires");
        }
        if(DancingAnimals.contains(selectAnimal)){
            selectedAttractions.add("Dancing Animals");
        }
        if(CountingMasters.contains(selectAnimal)){
            selectedAttractions.add("Counting Masters");
        }
        if(PassionateCoders.contains(selectAnimal)){
            selectedAttractions.add("Passionate Coders");
        }
        
        for(int i = 0; i<selectedAttractions.size(); i++){                
                System.out.println(i+1+". "+selectedAttractions.get(i));
            }
       
       
        System.out.println("Please choose your preferred attractions (type the number): ");
        int inputAttraction = scn.nextInt();
        String selectAttraction = selectedAttractions.get(inputAttraction-1);
        
        System.out.println("Wow, one more step,");
        System.out.println("please let us know your name: ");
        String inputName = scn.next();
        
        
        
        
        System.out.println("");
        System.out.println("Yeay, final check!");
        System.out.println("Here is your data, and the attraction you chose: \n");
        
        
        
        
        
        
        
        
             
   
        
        
    
    }
}